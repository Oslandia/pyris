# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

## [0.9.1] 2024-10-29

### Fixed

- Fix pyris front homepage (!63)

## [0.9.0] 2024-10-29

### Added

- i18n support (!51)
- Implement responsive charts (!55)
- Pre-commit (!58)
- Gitlab CI with linting and unit tests (!59)

### Changed

- Introduce Ansible to manage input data (!50)
- Include PostgreSQL installation to Ansible configuration (!51)
- Update Flask dependencies (!60)
- Update psycopg dependency (!61)

### Fixed

- Fix the API URL for production context (!49)
- Manage geocoding information during a session (!53)
- Unit tests (!54)
- Return a 404 error code if the address API returns an error (!57)

## [0.8.0] 2023-02-17

### Added

- Terms of use.

### Changed

- Migrate from flask-restplus to flask-restx. (!40)
- Migrate the front-end to React.js (!45)
- Update IRIS geometry data (2022). (!47)
- Update INSEE census data (2019). (!48)

### Fixed

- Fix IRIS searching function when the SQL query returns an empty result. (!36)
- Fix encoding with csvkit. (!37)
- Fix an echo command in Makefile. (!38)
- Fix an obsolete call to the pyyaml API. (!41)

## [0.7.1] 2020-07-28

### Added

- A `CHANGELOG` file.

### Changed

- Add a section "unit tests" to the README file.

### Fixed

- Encoding error (utf-8) in the Makefile when xls files were turned into csv files
  thanks to csvkit #36.
- Fix a 500 HTTP error when the result set of a SQL query was empty #35.


## [0.7.0] - 2020-04-03

The `CHANGELOG` was added after this version.


## [0.6.0] - 2018-03-08
