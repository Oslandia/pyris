Pyris
=====

IRIS Insee Geolocalizer

Pyris collects some data about
`IRIS <http://www.insee.fr/fr/methodes/default.asp?page=zonages/iris.htm>`__
from the French `INSEE <http://www.insee.fr/en/>`__ institute. It's
possible from an address to find the IRIS code and some **statistics
data**, e.g. population, employment or housing data.

`IRIS <http://www.insee.fr/fr/methodes/default.asp?page=zonages/iris.htm>`__
are specific codes and data related to more than 50,000 districts, built
by the `National Institute of Statistics and Economic
Studies <http://www.insee.fr/en/>`__.

Demo instance
-------------

You may run our demo instance at http://data.oslandia.io/pyris

**Terms of Use:**

This is a demo instance, please don't use in production. You can test
and prototype, but for a production instance please install your own
or contact us.

Interactive Dashboard
----------------------

A dashboard with a map and charts from Insee for each IRIS, census 2019: population (by sex and
age), employment and housing.

.. figure:: ./images/pyris-dashboard.png
   :alt: dashboard

   dashboard

REST API documentation
----------------------

.. figure:: ./images/pyris-doc-api.png
   :alt: api

   api

For instance, you can:

-  ``URL/iris/0104?limit=5`` to the some information about a specific
   IRIS code

-  ``URL/search/q=place de la bourse Bordeaux`` to get the IRIS data
   from a specific address

Data
----

You have to install postgreSQL and PostGIS. For Debian:

::

    sudo apt-get install postgresql postgis

You have to be a PostgreSQL superuser to create the postgis extension
for your database. If it's not the case, you can do:

-  ``su``
-  ``su - postgres``
-  ``psql pyris -c "CREATE EXTENSION postgis;"``

All usefull scripts are in the `scripts-data` folder. Two steps:

* Download and some data processing for IGN and Insee data. Take a look to the
  `ansible` in `scripts-data`.

* Then load data into your database thanks to the Python script `loader.py`.

To summarize, in a dedicated virtualenv, do:

1. Go to `scripts-data`
2. Run ansible `ansible-playbook --ask-become-pass ./ansible/playbook.yml` and enter the password for super user operations
3. Run `python loader.py ./data`

For Python dependencies which help you to process data, take a look to the
`scripts-data/requirements.txt` file.

You also have a configuration file example at `scripts-data/settings.ini.sample`.


Launch the Web App
------------------

First, install Node.js and npm. For Ubuntu:
::
    sudo apt install nodejs npm

Then, install modules for the web application:
::
    npm --prefix ./pyris/front install ./pyris/front

You should consider the `.env.development` file for modifying the web app port and the
`.env.production.sample` file so as to tune your own production instance.

Then, launch the API:
::
    gunicorn -b 127.0.0.1:5555 pyris.api.run:app

or if you have to specify db credentials or logging Flask app. See an `example of a app.yml
file <https://gitlab.com/oslandia/pyris/blob/master/app.yml>`__ configuration.
::
    gunicorn -b 127.0.0.1:5555 --env PYRIS_APP_SETTINGS=./appdev.yml pyris.api.run:app

Then, launch the web application:
::
    npm start --prefix pyris/front/

Visit http://localhost:3000/home


Launch the Unit Tests
---------------------

You must set the database and have a YAML configuration inspired from ``app.yml`` (db
name and credentials, etc.). You should copy the ``app.yml`` file and modify it. Then,
you can use ``pytest``:
::
    PYRIS_APP_SETTINGS=appdev.yml pytest -v -rsf
