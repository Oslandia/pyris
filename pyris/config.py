"""Pyris configuration

Retrieve user and password from the YAML configuration file for the database
access
"""

import os

import yaml
from yaml import load as yload

_cfgfile = os.environ.get("PYRIS_APP_SETTINGS")

if _cfgfile is not None:
    with open(_cfgfile, "r") as fobj:
        conf = yload(fobj.read(), yaml.Loader)
        DATABASE = conf.get("database", {})
        geocoder_conf = conf.get("geocoder")
        if geocoder_conf:
            GEOCODER_URL = geocoder_conf["url"]
        else:
            GEOCODER_URL = "http://api-adresse.data.gouv.fr"
        URL_SUFFIX = conf.get("deployment", {}).get("URL_SUFFIX", "")
else:
    DATABASE = {
        "DBNAME": "pyris",
        "HOST": "localhost",
        "USER": os.environ["USER"],
    }
    GEOCODER_URL = "http://api-adresse.data.gouv.fr"
    URL_SUFFIX = ""
