import React from "react";
import "../styles/App.css";
import ResponsiveAppBar from "./AppBar";
import Home from "./Home";
import Dashboard from "./Dashboard";
import { Route, Routes } from "react-router";
import { Navigate, useLocation } from "react-router-dom";
import { useTranslation } from 'react-i18next';

const { defaultLanguage, availableLanguages, } = require("../settings");

function App() {
   const { t, i18n } = useTranslation();
   const location = useLocation();

   // Check language in the url
   const locationPathname = location.pathname;
   // Extract the language specified
   const language = locationPathname.split("/", 2)[1];
   if (availableLanguages.includes(language) === false) {
    localStorage.setItem("i18nextLng", i18n.resolvedLanguage);
   } else {
    localStorage.setItem("i18nextLng", language);
    if (i18n.resolvedLanguage !== language) {
        i18n.changeLanguage(language);
       };
   }

   return (
       <div className="App">
           <div>
               <ResponsiveAppBar />
               <div className="map-dashboard-container">
                   <Routes>
                       <Route path={`/${localStorage.getItem("i18nextLng")}/${t('description.urlTabHome', { lng: defaultLanguage})}`} exact element={<Home />} />
                       <Route path={`/${localStorage.getItem("i18nextLng")}/${t('description.urlTabDashboard', { lng: defaultLanguage})}`} element={<Dashboard />}>
                           <Route path=":irisCode" element={<Dashboard />} />
                       </Route>
                       <Route
                           path={`/*`}
                           element={<Navigate replace to={`/${localStorage.getItem("i18nextLng")}/${t('description.urlTabHome', { lng: defaultLanguage})}`} />}
                       ></Route>
                   </Routes>
               </div>
           </div>
       </div>
   );
}

export default App;
