import MenuIcon from "@mui/icons-material/Menu";
import AppBar from "@mui/material/AppBar";
import "../styles/AppBar.css";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import * as React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Select from "@mui/material/Select";
import { useLocation, useNavigate } from "react-router-dom";

const { defaultLanguage } = require("../settings");


const ResponsiveAppBar = () => {
    const location = useLocation();
    const navigate = useNavigate();

    const [anchorElNav, setAnchorElNav] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const { t, i18n } = useTranslation();
    const pages = [t("description.tabHome"), t("description.tabDashboard")];

    // Available languages
    const listLanguages = {
      "fr": t("description.languageFrench"),
      "en": t("description.languageEnglish")
    }
    const currentLanguage = listLanguages[i18n.resolvedLanguage];

    // Update of the language and of the url
    const handleLanguageChange = (event) => {
        let languageName = event.target.value;
        let lng = "";

        // Translation in the url
        let loc = location.pathname;
        if (languageName === t("description.languageFrench")) {
          lng = "fr";
          loc = loc.replace("en", lng);
        }
        else {
          lng = "en";
          loc = loc.replace("fr", lng);
        }
        localStorage.setItem("i18nextLng", lng);
        i18n.changeLanguage(lng);

        navigate(loc);

        // Refresh the placeholder translation
        if ([t("description.urlTabDashboard", { lng: defaultLanguage})].some(value => loc.includes(value))) {
            window.location.reload();
        }
    };

    return (
        <AppBar
            sx={{
                position: "static",
                backgroundColor: "#364F6B",
                height: "68px",
            }}
        >
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Typography
                        variant="h6"
                        noWrap
                        component="a"
                        href="/"
                        sx={{
                            mr: 2,
                            display: { xs: "none", md: "flex" },
                            fontFamily: "monospace",
                            fontWeight: 700,
                            letterSpacing: ".3rem",
                            color: "inherit",
                            textDecoration: "none",
                        }}
                    ></Typography>
                    <Box
                        sx={{
                            flexGrow: 1,
                            display: { xs: "flex", md: "none" },
                        }}
                    >
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: "bottom",
                                horizontal: "left",
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "left",
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: "block", md: "none" },
                            }}
                        >
                            {pages.map((page) => (
                                <MenuItem
                                    key={page}
                                    onClick={handleCloseNavMenu}
                                >
                                    <Typography textAlign="center">
                                        <Link
                                            style={{
                                                textDecoration: "none",
                                                color: "white",
                                            }}
                                            to={`/${page}`}
                                        >
                                            {page}
                                        </Link>
                                    </Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href=""
                        sx={{
                            mr: 2,
                            display: { xs: "flex", md: "none" },
                            flexGrow: 1,
                            fontFamily: "monospace",
                            fontWeight: 700,
                            letterSpacing: ".3rem",
                            color: "inherit",
                            textDecoration: "none",
                        }}
                    ></Typography>
                    <Box
                        sx={{
                            flexGrow: 1,
                            display: { xs: "none", md: "flex" },
                        }}
                        className="menu-indicators"
                    >
                        {pages.map((page) => (
                            <Button
                                key={page}
                                onClick={handleCloseNavMenu}
                                sx={{ my: 2, color: "white", display: "block" }}
                            >
                                <Link
                                    style={{
                                        textDecoration: "none",
                                        color: "white",
                                    }}
                                    to={`/${localStorage.getItem("i18nextLng")}/${page
                                        .toLowerCase()
                                        .replace(/ /g, "-")
                                        .replace("home", t("description.urlTabHome", { lng: defaultLanguage}))
                                        .replace("dashboard", t("description.urlTabDashboard", { lng: defaultLanguage}))
                                        }`}
                                >
                                    {page}
                                </Link>
                            </Button>
                        ))}
                    </Box>
                    <div id="box-select-language">
                        <Select
                            id="appbar-select-language"
                            defaultValue={currentLanguage}
                            value={currentLanguage}
                            onChange={handleLanguageChange}
                        >
                            {Object.values(listLanguages).map((languageName, i) => {
                                return (
                                    <MenuItem value={languageName} key={i}>
                                        {languageName}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </div>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default ResponsiveAppBar;
