import React, { useEffect, useRef, useState } from "react";
import Chart from "chart.js/auto";
import { useSelector } from "react-redux";
import store from "../store";

function Charts(props) {
    const chartSection = props.data.section;
    const chartName = props.data.chart;

    const [dataChart] = useState(
        store.getState().charts[chartSection].charts[chartName]
    );
    const storeCharts = useSelector((state) => store.getState().charts);

    const chartRef = useRef(null);
    const [myChart, setMyChart] = useState(null);

    useEffect(() => {
        if (!chartRef) return;
        const ctx = chartRef.current.getContext("2d");
        const myChart = new Chart(ctx, {
            type: dataChart.typeChart,
            data: {
                labels: dataChart.labelsChart,
                datasets: dataChart.datasets,
            },
            height: null,
            width: null,
            options: {
                responsive: true,
                maintainAspectRatio: dataChart.maintainAspectRatio,
                interaction: {
                    mode: "index",
                },
                plugins: {
                    legend: {
                        display:
                            dataChart.legendDisplay === "true" &&
                            dataChart.legendCustom !== "true",
                        position: "bottom",
                    },
                    title: {
                        display: dataChart.titleDisplay === "true",
                        text: dataChart.titleChart,
                        font: {
                            size: "12",
                        },
                    },
                },
            },
        });
        setMyChart(myChart);
    }, [chartRef]);

    useEffect(() => {
        if (!myChart) return;
        myChart.data.datasets = store.getState().charts[chartSection].charts[
            chartName
        ].datasets;
        myChart.update();
    }, [myChart, storeCharts]);

    return (
        <canvas
                ref={chartRef}
            />
    );
}

export default Charts;
