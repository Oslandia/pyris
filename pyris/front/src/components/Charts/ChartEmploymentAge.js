import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import store from "../../store";
import Chart from "../Chart";
import { updateChartData } from "../../stores/Charts";

export default function ChartPopulationAge(props) {
    const chartSection = props.data.section;
    const chartName = props.data.chart;
    const container = store.getState().charts[chartSection].charts[chartName].container;
    const containerHeight = container.height;
    const containerWidth = container.width;

    const dispatch = useDispatch();
    const storeIris = useSelector((state) => store.getState().iris);
    const completeCodeIris = storeIris.currentCompleteCode;

    const [chartData, setChartData] = useState(
        store.getState().charts[chartSection].charts[chartName]
    );

    // Update the data stored in the store and the state when a new iris is selected
    useEffect(() => {
        getData();
    }, [completeCodeIris]);

    // Get the data associated with the current iris selected
    const getData = () => {
        chartData.datasets.forEach((dataset, index) => {
            axios
                .get(dataset.url + completeCodeIris + dataset.urlParams)
                .then(function(response) {
                    if (response.status === 200) {
                        const data = response.data.data;

                        let dataDataset = [];
                        // Active
                        if (index === 0) {
                            dataDataset = [
                                Math.round(data.actif_15_24),
                                Math.round(data.actif_25_54),
                                Math.round(data.actif_55_64),
                            ]
                        }
                        // Unemployed
                        else {
                            dataDataset = [
                                Math.round(data.chomage_15_24),
                                Math.round(data.chomage_25_54),
                                Math.round(data.chomage_55_64),
                            ]
                        }

                        dispatch(
                            updateChartData({
                                section: chartSection,
                                chart: chartName,
                                index: index,
                                data: dataDataset
                            })
                        );
                        setChartData({
                            ...chartData,
                            datasets: store.getState().charts[chartSection]
                                .charts[chartName].datasets,
                        });
                    }
                });
        });
    };

    return (
        <div style={{ height: `${containerHeight}vh`, width: `${containerWidth}vw`, position: "relative" }}>
            <Chart data={props.data} />
        </div>
    );
}
