import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import store from "../../store";
import Chart from "../Chart";
import { updateChartData } from "../../stores/Charts";

export default function ChartPopulationSex(props) {
    const chartSection = props.data.section;
    const chartName = props.data.chart;
    const container = store.getState().charts[chartSection].charts[chartName].container;
    const containerHeight = container.height;
    const containerWidth = container.width;

    const dispatch = useDispatch();
    const storeIris = useSelector((state) => store.getState().iris);
    const completeCodeIris = storeIris.currentCompleteCode;

    const [chartData, setChartData] = useState(
        store.getState().charts[chartSection].charts[chartName]
    );

    // Update the data stored in the store and the state when a new iris is selected
    useEffect(() => {
        getData();
    }, [completeCodeIris]);

    // Get the data associated with the current iris selected
    const getData = () => {
        chartData.datasets.forEach((dataset, index) => {
            axios
                .get(dataset.url + completeCodeIris + dataset.urlParams)
                .then(function(response) {
                    if (response.status === 200) {
                        const data = response.data.data;

                        let dataDataset = [];
                        // Female
                        if (index === 0) {
                            dataDataset = [
                                Math.round(data.femme_actif),
                                Math.round(data.femme_inactif),
                                Math.round(data.femme_chomage),
                                Math.round(data.femme_etudiant),
                                Math.round(data.femme_retraite),
                            ]
                        }
                        // Male
                        else {
                            dataDataset = [
                                Math.round(data.homme_actif),
                                Math.round(data.homme_inactif),
                                Math.round(data.homme_chomage),
                                Math.round(data.homme_etudiant),
                                Math.round(data.homme_retraite),
                            ]
                        }

                        dispatch(
                            updateChartData({
                                section: chartSection,
                                chart: chartName,
                                index: index,
                                data: dataDataset
                            })
                        );
                        setChartData({
                            ...chartData,
                            datasets: store.getState().charts[chartSection]
                                .charts[chartName].datasets,
                        });
                    }
                });
        });
    };

    return (
        <div style={{ height: `${containerHeight}vh`, width: `${containerWidth}vw`, position: "relative" }}>
            <Chart data={props.data} />
        </div>
    );
}
