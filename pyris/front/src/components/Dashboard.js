import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import "../styles/Dashboard.css";
import MapWrapper from "./Map";
import DashboardIris from "./Dashboards/DashboardIris";
import DashboardPopulation from "./Dashboards/DashboardPopulation";
import DashboardHousing from "./Dashboards/DashboardHousing";
import DashboardEmployment from "./Dashboards/DashboardEmployment";
import store from "../store";
import KeyFigurePopulationTotal from "./KeyFigures/KeyFigurePopulationTotal";
import Alert from "@mui/material/Alert";
import Stack from "@mui/material/Stack";
import Snackbar from "@mui/material/Snackbar";
import axios from "axios";
import { updateIrisCodes } from "../stores/Iris";
import { updateMapLoad } from "../stores/Map";
import { useTranslation } from 'react-i18next';
import * as turf from "@turf/turf";

const { api_url, url_suffix, defaultLanguage } = require("../settings");

function Dashboard() {
    const { t } = useTranslation();
    const urlTabDashboard = t('description.urlTabDashboard', { lng: defaultLanguage});
    const language = localStorage.getItem("i18nextLng");

    const dispatch = useDispatch();
    const location = useLocation();
    const urlParams = useParams();
    const navigate = useNavigate();

    const [alert, setAlert] = useState({ open: false, message: "" });
    const [vertical] = useState("top");
    const [horizontal] = useState("center");
    // Close the alert indicating a bad iris code in the url
    const closeAlert = () => {
        setAlert({ open: false, message: "" });
    };

    const storeIris = useSelector((state) => store.getState().iris);
    const storeIrisCurrentCode = storeIris.currentCode;
    const visibilityContainer =
        storeIrisCurrentCode === "" ? "hidden" : "visible";
    const opacityContainer = storeIrisCurrentCode === "" ? "0" : "1";

    // Check the validity of the iris code in the url
    useEffect(() => {
        // Put load property to false in the Map store to avoid a problem loading of the Map component
        if (location.pathname === url_suffix + `/${language}/${urlTabDashboard}`) {
            dispatch(updateMapLoad(false));
        }

        const urlParamIrisCode = urlParams.irisCode;
        if (typeof urlParamIrisCode !== "undefined") {
            if (urlParamIrisCode.length !== 9) {
                setAlert({
                    open: true,
                    message:
                        `The iris code in the url must have 9 characters. \nRedirection to /${language}/${urlTabDashboard}`,
                });
                navigate(`/${language}/${urlTabDashboard}`);
            } else {
                axios
                    .get(api_url + "/compiris/" + urlParamIrisCode, {
                        params: {
                            geojson: false,
                        },
                    })
                    .then(function (response) {
                        if (response.status === 200) {
                            // Update of the current iris code
                            const responseData = response.data;
                            const responseDataIris = responseData.iris;
                            const responseDataIrisCompleteCode =
                                responseData.complete_code;
                            const responseDataIrisName = responseData.name;
                            const responseDataIrisCity = responseData.city;
                            const geocoderAddress =
                                responseDataIrisName +
                                ", " +
                                responseDataIrisCity;

                            // New iris entered in the search bar
                            // Remove the geocoder attributes stored in the session storage ()
                            if (responseDataIris !== storeIris.currentCode) {
                                let urlApi =
                                    api_url +
                                    "/compiris/" +
                                    responseDataIrisCompleteCode;
                                let urlApiParams = {
                                    geojson: true,
                                };
                                axios
                                    .get(urlApi, {
                                        params: urlApiParams,
                                    })
                                    .then(function (response) {
                                        if (response.status === 200) {
                                            const feature = response.data;

                                            // Update the center and zoom values of the map in the store -> useEffect function
                                            let centroid = turf.center(feature);
                                            let centroidCoordinates = [
                                                centroid.geometry
                                                    .coordinates[1],
                                                centroid.geometry
                                                    .coordinates[0],
                                            ];
                                            sessionStorage.setItem(
                                                "geocoderAddress",
                                                geocoderAddress
                                            );
                                            sessionStorage.setItem(
                                                "geocoderLongitude",
                                                centroidCoordinates[0]
                                            );
                                            sessionStorage.setItem(
                                                "geocoderLatitude",
                                                centroidCoordinates[1]
                                            );
                                        } else {
                                            sessionStorage.setItem(
                                                "geocoderAddress",
                                                ""
                                            );
                                            sessionStorage.setItem(
                                                "geocoderLongitude",
                                                ""
                                            );
                                            sessionStorage.setItem(
                                                "geocoderLatitude",
                                                ""
                                            );
                                        }
                                    });
                            }
                            dispatch(
                                updateIrisCodes({
                                    currentCode: responseDataIris,
                                    currentCompleteCode:
                                        responseDataIrisCompleteCode,
                                })
                            );
                        }
                    })
                    .catch(function (error) {
                        if (error.response.status === 404) {
                            setAlert({
                                open: true,
                                message:
                                    `The iris code in the url does not exist. \nRedirection to /${language}/${urlTabDashboard}`,
                            });
                            navigate(`/${language}/${urlTabDashboard}`);
                        }
                    });
            }
        }
    }, [urlParams, location.pathname, dispatch, language, navigate, urlTabDashboard]);

    return (
        <div className="dashboard-container">
            <div>
                {/* Display an error when the iris code in the url is wrong*/}
                <div>
                    <Stack spacing={2} sx={{ width: "100%" }}>
                        <Snackbar
                            anchorOrigin={{ vertical, horizontal }}
                            open={alert.open}
                            autoHideDuration={5000}
                            onClose={closeAlert}
                        >
                            <Alert
                                onClose={closeAlert}
                                severity="error"
                                sx={{ width: "100%" }}
                            >
                                {alert.message}
                            </Alert>
                        </Snackbar>
                    </Stack>
                </div>
                <div
                    // id="map-container"
                    id={
                        storeIris.currentCode === ""
                            ? "map-container-full-screen"
                            : "map-container"
                    }
                    className={storeIris.currentCode === "" ? "" : "container"}
                    style={{ visibility: "visible", opacity: "1" }}
                >
                    <MapWrapper />
                </div>
                {/* API call only if there is an iris code in the store */}
                {storeIrisCurrentCode !== "" ? (
                    <div>
                        {" "}
                        <div
                            id="iris-container"
                            className="container"
                            style={{
                                visibility: visibilityContainer,
                                opacity: opacityContainer,
                            }}
                        >
                            <div className="icon-label-container">
                                <p className="icon-container icon-iris-container"></p>
                                <p className="label-container">{t('description.dashboardIris')}</p>
                            </div>
                            <DashboardIris data={{ section: "Iris" }}/>
                        </div>
                        <div
                            id="population-container"
                            className="container"
                            style={{
                                visibility: visibilityContainer,
                                opacity: opacityContainer,
                            }}
                        >
                            <div className="icon-label-container">
                                <p className="icon-container icon-population-container"></p>
                                <p className="label-container">{t('description.dashboardPopulation')}</p>
                            </div>
                            <KeyFigurePopulationTotal />
                            <DashboardPopulation data={{ section: "Population" }}/>
                        </div>
                        <div
                            id="housing-container"
                            className="container"
                            style={{
                                visibility: visibilityContainer,
                                opacity: opacityContainer,
                            }}
                        >
                            <div className="icon-label-container">
                                <p className="icon-container icon-housing-container"></p>
                                <p className="label-container">{t('description.dashboardHousing')}</p>
                            </div>
                            <DashboardHousing data={{ section: "Housing" }} />
                        </div>
                        <div
                            id="employment-container"
                            className="container"
                            style={{
                                visibility: visibilityContainer,
                                opacity: opacityContainer,
                            }}
                        >
                            <div className="icon-label-container">
                                <p className="icon-container icon-employment-container"></p>
                                <p className="label-container">{t('description.dashboardEmployment')}</p>
                            </div>
                            <DashboardEmployment data={{ section: "Employment" }} />
                        </div>{" "}
                    </div>
                ) : (
                    ""
                )}
            </div>
        </div>
    );
}

export default Dashboard;
