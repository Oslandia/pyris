import React from "react";
import "../../styles/Dashboards/DashboardEmployment.css";
import KeyFigureEmployment from "../KeyFigures/KeyFigureEmployment";
import ChartEmployment from "../Charts/ChartEmployment";
import ChartEmploymentSex from "../Charts/ChartEmploymentSex";
import ChartEmploymentAge from "../Charts/ChartEmploymentAge";
import store from "../../store";

export default function DashboardEmployment(props) {
    const dashboardSection = props.data.section;
    const containers = store.getState().dashboards[dashboardSection].containers;
    const containerOne = containers["one"];
    const containerOneHeight = containerOne.height;
    const containerOneWidth = containerOne.width;

    return (
        <div>
            <div
                className="dashboard-employment-container-one"
                style={{
                    height: `${containerOneHeight}vh`,
                    width: `${containerOneWidth}vw`,
                }}
            >
                <KeyFigureEmployment
                    data={{ section: "Employment", id: "one" }}
                />
                <ChartEmployment
                    data={{ section: "Employment", chart: "employment" }}
                />
                <ChartEmploymentSex
                    data={{ section: "Employment", chart: "employment_sex" }}
                />
                <ChartEmploymentAge
                    data={{ section: "Employment", chart: "employment_age" }}
                />
            </div>
        </div>
    );
}
