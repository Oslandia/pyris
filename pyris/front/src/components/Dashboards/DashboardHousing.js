import React from "react";
import "../../styles/Dashboards/DashboardHousing.css";
import ChartHousingType from "../Charts/ChartHousingType";
import ChartHousingYear from "../Charts/ChartHousingYear";
import ChartHousingRoom from "../Charts/ChartHousingRoom";
import ChartHousingArea from "../Charts/ChartHousingArea";
import store from "../../store";

export default function DashboardHousing(props) {
    const dashboardSection = props.data.section;
    const containers = store.getState().dashboards[dashboardSection].containers;
    const containerOne = containers["one"];
    const containerOneHeight = containerOne.height;
    const containerOneWidth = containerOne.width;

    return (
        <div>
            <div
                className="dashboard-housing-container-one"
                style={{
                    height: `${containerOneHeight}vh`,
                    width: `${containerOneWidth}vw`,
                }}
            >
                <ChartHousingType
                    data={{ section: "Housing", chart: "type" }}
                />
                <ChartHousingYear
                    data={{ section: "Housing", chart: "year" }}
                />
                <ChartHousingRoom
                    data={{ section: "Housing", chart: "room" }}
                />
                <ChartHousingArea
                    data={{ section: "Housing", chart: "area" }}
                />
            </div>
        </div>
    );
}
