import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import "../../styles/Dashboards/DashboardIris.css";
import axios from "axios";
import store from "../../store";
import { useTranslation } from "react-i18next";

const { api_url } = require("../../settings");

export default function DashboardIris(props) {
    const { t } = useTranslation();

    const dashboardSection = props.data.section;
    const containers = store.getState().dashboards[dashboardSection].containers;
    const containerOne = containers["one"];
    const containerOneHeight = containerOne.height;
    const containerOneWidth = containerOne.width;
    const containerOneOverflow = containerOne.overflow;

    const storeIris = useSelector((state) => store.getState().iris);
    const completeCodeIris = storeIris.currentCompleteCode;

    const [irisData, setIrisData] = useState({
        code: "",
        name: "",
        city: "",
        citycode: "",
        type: "",
    });

    // Update the data stored in the state when a new iris is selected
    useEffect(() => {
        getDataIris();
    }, [completeCodeIris]);

    // Get the data associated with the current iris selected
    const getDataIris = () => {
        axios
            .get(api_url + "/compiris/" + completeCodeIris)
            .then(function (response) {
                if (response.status === 200) {
                    const data = response.data;
                    setIrisData({
                        ...irisData,
                        code: data.iris,
                        name: data.name,
                        city: data.city,
                        citycode: data.citycode,
                        type: data.type,
                    });
                }
            });
    };

    return (
        <div
            className="dashboard-iris-container-one"
            style={{
                height: `${containerOneHeight}vh`,
                width: `${containerOneWidth}vw`,
                overflow: containerOneOverflow,
            }}
        >
            <div className="description-iris-container">
                <div className="description-iris-row ">
                    <p className="description-iris-label">
                        {t("description.chartIrisCode")}
                    </p>
                    <p className="description-iris-value">{irisData.code}</p>
                </div>
                <div className="description-iris-row ">
                    <p className="description-iris-label">
                        {t("description.chartIrisName")}
                    </p>
                    <p className="description-iris-value">{irisData.name}</p>
                </div>
                <div className="description-iris-row ">
                    <p className="description-iris-label">
                        {t("description.chartIrisCity")}
                    </p>
                    <p className="description-iris-value">{irisData.city}</p>
                </div>
                <div className="description-iris-row ">
                    <p className="description-iris-label">
                        {t("description.chartIrisCitycode")}
                    </p>
                    <p className="description-iris-value">
                        {irisData.citycode}
                    </p>
                </div>
                <div className="description-iris-row ">
                    <p className="description-iris-label">
                        {t("description.chartIrisType")}
                    </p>
                    <p className="description-iris-value">{irisData.type}</p>
                </div>
            </div>
        </div>
    );
}
