import React from "react";
import "../../styles/Dashboards/DashboardPopulation.css";
import ChartPopulation from "../Charts/ChartPopulation";
import ChartPopulationAge from "../Charts/ChartPopulationAge";
import ChartPopulationAgeSex from "../Charts/ChartPopulationAgeSex";
import store from "../../store";

export default function DashboardPopulation(props) {
    const dashboardSection = props.data.section;
    const containers = store.getState().dashboards[dashboardSection].containers;
    const containerOne = containers["one"];
    const containerOneHeight = containerOne.height;
    const containerOneWidth = containerOne.width;
    const containerTwo = containers["two"];
    const containerTwoHeight = containerTwo.height;
    const containerTwoWidth = containerTwo.width;

    return (
        <div>
            <div
                className="dashboard-population-container-one"
                style={{
                    height: `${containerOneHeight}vh`,
                    width: `${containerOneWidth}vw`,
                }}
            >
                <ChartPopulation
                    data={{ section: "Population", chart: "population" }}
                />
                <ChartPopulationAge
                    data={{ section: "Population", chart: "population_age" }}
                />
            </div>
            <div
                className="dashboard-population-container-two"
                style={{
                    height: `${containerTwoHeight}vh`,
                    width: `${containerTwoWidth}vw`,
                }}
            >
                <ChartPopulationAgeSex
                    data={{
                        section: "Population",
                        chart: "population_age_sex",
                    }}
                />
            </div>
        </div>
    );
}
