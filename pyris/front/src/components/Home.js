import React from "react";
import "../styles/Home.css";
import { useTranslation } from 'react-i18next';
import parse from "html-react-parser";

function Home() {
        const { t } = useTranslation();

        return (
            <div className="home-container">
                <p className="main-title">{t('description.homeTitleMain')}</p>

                <div className="home-description-container">
                    <p className="title">{t('description.homeTitleDescription')}</p>
                    {parse(t('description.homeTextDescription'))}
                    <p className="title">{t('description.homeTitleInteractiveDashboard')}</p>
                    {parse(t('description.homeTextInteractiveDashboard'))}
                    <p className="title">{t('description.homeTitleData')}</p>
                    {parse(t('description.homeTextData'))}
                    <p className="title">{t('description.homeTitleCode')}</p>
                    {parse(t('description.homeTextCode'))}
                    <p className="title">{t('description.homeTitleAccess')}</p>
                    {parse(t('description.homeTextAccess'))}
                </div>
            </div>
        );
}

export default Home;
