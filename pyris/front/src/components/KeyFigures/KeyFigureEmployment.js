import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import "../../styles/KeyFigures/KeyFigureEmployment.css";
import axios from "axios";
import store from "../../store";
import { useTranslation } from 'react-i18next';

const { api_url } = require("../../settings");

export default function KeyFigureEmployment(props) {
    const { t } = useTranslation();

    const keyFigureSection = props.data.section;
    const keyFigureId = props.data.id;
    const keyFigure = store.getState().keyfigures[keyFigureSection].keyFigures[keyFigureId];
    const keyFigureHeight = keyFigure.height;
    const keyFigureOverflow = keyFigure.overflow;

    const storeIris = useSelector((state) => store.getState().iris);
    const completeCodeIris = storeIris.currentCompleteCode;

    const [employmentData, setEmploymentData] = useState({
        population_15_64: "",
        active: "",
        unemployed: "",
        unemployment_rate: "",
    });

    // Update the data stored in the state when a new iris is selected
    useEffect(() => {
        getData();
    }, [completeCodeIris]);

    // Get the data associated with the current iris selected
    const getData = () => {
        axios
            .get(api_url + "/insee/activite/" + completeCodeIris)
            .then(function(response) {
                if (response.status === 200) {
                    const data = response.data;

                    setEmploymentData({
                        ...employmentData,
                        population_15_64: Math.round(data.population),
                        active: Math.round(data.actif),
                        unemployed: Math.round(data.chomeur),
                        unemployment_rate: Math.round(data.taux_chomage) + "%",
                    });
                }
            });
    };

    return (
        <div className="key-figure-employment-container" style={{ height: `${keyFigureHeight}vh`, overflow: keyFigureOverflow }}>
            <div className="key-figure-employment-row">
                <p className="key-figure-employment-label">{t('description.dashboardEmploymentPopulation1564')}</p>
                <p className="key-figure-employment-value">{employmentData.population_15_64}</p>
            </div>
            <div className="key-figure-employment-row">
                <p className="key-figure-employment-label">{t('description.dashboardEmploymentActive')}</p>
                <p className="key-figure-employment-value">{employmentData.active}</p>
            </div>
            <div className="key-figure-employment-row">
                <p className="key-figure-employment-label">{t('description.dashboardEmploymentUnemployed')}</p>
                <p className="key-figure-employment-value">{employmentData.unemployed}</p>
            </div>
            <div className="key-figure-employment-row">
                <p className="key-figure-employment-label">{t('description.dashboardEmploymentUnemploymentRate')}</p>
                <p className="key-figure-employment-value">{employmentData.unemployment_rate}</p>
            </div>
        </div>
    );
}
