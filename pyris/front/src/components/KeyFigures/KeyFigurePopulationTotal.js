import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import "../../styles/KeyFigures/KeyFigurePopulationTotal.css";
import axios from "axios";
import store from "../../store";

const { api_url } = require("../../settings");

export default function KeyFigurePopulationTotal() {
    const storeIris = useSelector((state) => store.getState().iris);
    const completeCodeIris = storeIris.currentCompleteCode;

    const [populationData, setPopulationData] = useState({
        total: "",
    });

    // Update the data stored in the state when a new iris is selected
    useEffect(() => {
        getData();
    }, [completeCodeIris]);

    // Get the data associated with the current iris selected
    const getData = () => {
        axios
            .get(api_url + "/insee/population/" + completeCodeIris)
            .then(function(response) {
                if (response.status === 200) {
                    const data = response.data;
                    setPopulationData({
                        ...populationData,
                        total: Math.round(data.population),
                    });
                }
            });
    };

    return (
        <div className="key-figure-population-container">
            <p>Total: </p>
            <p className="key-figure-population-value">{populationData.total}</p>
        </div>
    );
}
