import React, { useEffect, useRef, useState } from "react";
import "../styles/Map.css";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import "leaflet-geocoder-ban/dist/leaflet-geocoder-ban.min.css";
import "leaflet-geocoder-ban/dist/leaflet-geocoder-ban.js";
import store from "../store";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import {
    updateMapCenter,
    updateMapZoom,
    updateGeocoderAttributes,
} from "../stores/Map";
import { updateIrisCodes } from "../stores/Iris";
import { withResizeDetector } from "react-resize-detector";
import * as turf from "@turf/turf";
import { useNavigate } from "react-router-dom";
import { useTranslation } from 'react-i18next';

const { api_url, defaultLanguage } = require("../settings");

function MapWrapper() {
    const { t } = useTranslation();
    const urlTabDashboard = t('description.urlTabDashboard', { lng: defaultLanguage});
    const language = localStorage.getItem("i18nextLng");

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const storeIris = useSelector((_state) => store.getState().iris);
    const completeCodeIris = storeIris.currentCompleteCode;
    const storeMap = useSelector((_state) => store.getState().map);

    const [mapLoad, changeMapLoad] = useState(false);
    const [mapZoom, changeMapZoom] = useState(storeMap.zoom);
    const [mapCenter, changeMapCenter] = useState(storeMap.center);

    let width = storeIris.currentCode === "" ? "100%" : "40%";

    let mapRef = useRef(null);
    const tileLayerOsm = L.tileLayer(
        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        {
            maxZoom: 19,
            attribution:
                '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        }
    );
    const leafIcon = L.Icon.extend({
        options: {
            iconSize: [35, 35],
            iconAnchor: [15, 30],
            popupAnchor: [0, -25],
        },
    });

    // BAN Geocoder
    const iconGeocoderMarker = new leafIcon({
        iconUrl: require("../img/marker_geocoder.png"),
    });
    let geocoderMarkerLayer = "";
    let irisLayer = "";
    const geocoder = L.geocoderBAN({
        placeholder: t('description.mapGeocoding'),
        resultsNumber: 5,
        collapsed: false,
        style: "searchBar",
    });
    geocoder.markGeocode = function (feature) {
        retrieveGeocodingResult(feature);
    };

    // Add the iris layer if a value is present in the store
    useEffect(() => {
        if (completeCodeIris !== "") {
            addIrisGeocoderMarkerLayer("", "", true);
        }
    }, [completeCodeIris]);

    // Loading the map and updating the view
    useEffect(() => {
        // Initial loading of the map
        if (mapLoad === false) {
            mapRef.current = L.map("map");
            tileLayerOsm.addTo(mapRef.current);
            geocoder.addTo(mapRef.current);
            mapRef.current.zoomControl.setPosition("bottomleft");
            updateMapView();
            changeMapLoad(true);
        }
        // Update of the map view when an address has been geocoded
        else {
            updateMapView();
        }
        mapRef.current.invalidateSize();
    }, [width, mapCenter, geocoder, mapLoad, tileLayerOsm]);

    // Update the map view
    const updateMapView = () => {
        if (storeMap.load === false) {
            mapRef.current.setView(mapCenter, mapZoom);
        }
        // Map is already loaded
        else {
            mapRef.current.flyTo(mapCenter, mapZoom, {
                animate: true,
                duration: 1,
            });
        }
    };

    // Remove geocoder and iris layers
    const removeAllLayers = () => {
        mapRef.current.eachLayer(function (layer) {
            let urlLayer = layer._url;
            if (!urlLayer) {
                mapRef.current.removeLayer(layer);
            }
        });
    };

    // Retrieve the geocoding result
    const retrieveGeocodingResult = (feature) => {
        const latitudeGeocoderCoordinate = feature.geometry.coordinates[1];
        const longitudeGeocoderCoordinate = feature.geometry.coordinates[0];
        let geocoderCoordinates = [
            latitudeGeocoderCoordinate,
            longitudeGeocoderCoordinate,
        ];
        const geocoderLabel = feature.properties.label;

        // Add layers
        removeAllLayers();
        addIrisGeocoderMarkerLayer(geocoderCoordinates, geocoderLabel, false);
    };

    // Add the geocoder marker layer
    const addGeocoderMarkerLayer = (geocoderCoordinates, geocoderLabel) => {
        geocoderMarkerLayer = L.marker(geocoderCoordinates, {
            icon: iconGeocoderMarker,
        });
        geocoderMarkerLayer.addTo(mapRef.current);

        // Change the geocoder current text
        document.querySelector(
            ".leaflet-control-geocoder-ban-form input"
        ).value = geocoderLabel;

        let geocoderLongitude = geocoderCoordinates[0];
        let geocoderLatitude = geocoderCoordinates[1];
        // Store geocoder attributes in Redux store
        dispatch(
            updateGeocoderAttributes({
                address: geocoderLabel,
                longitude: geocoderLongitude,
                latitude: geocoderLatitude,
            })
        );
        // Store geocoder attributes in sessionStorage
        sessionStorage.setItem("geocoderAddress", geocoderLabel);
        sessionStorage.setItem("geocoderLongitude", geocoderLongitude);
        sessionStorage.setItem("geocoderLatitude", geocoderLatitude);
    };

    // Retrieve iris coordinates associated with the geocoded adress or the iris entered in the url
    // Add iris layer
    const addIrisGeocoderMarkerLayer = (
        geocoderCoordinates,
        geocoderLabel,
        useGeocoderAttributesSessionStorage = false
    ) => {
        let urlApi = "";
        let urlApiParams = {};
        // Geocoded adress
        if (geocoderCoordinates !== "") {
            urlApi = api_url + "/coords";
            urlApiParams = {
                lat: geocoderCoordinates[0],
                lon: geocoderCoordinates[1],
                geojson: true,
            };
        }
        // Iris entered in the url
        else {
            urlApi = api_url + "/compiris/" + completeCodeIris;
            urlApiParams = {
                geojson: true,
            };
            navigate(`/${language}/${urlTabDashboard}/` + completeCodeIris);
        }

        axios
            .get(urlApi, {
                params: urlApiParams,
            })
            .then(function (response) {
                if (response.status === 200) {
                    const feature = response.data;
                    const featureProperties = feature.properties;

                    // Update the center and zoom values of the map in the store -> useEffect function
                    let centroid = turf.center(feature);
                    let centroidCoordinates = [
                        centroid.geometry.coordinates[1],
                        centroid.geometry.coordinates[0],
                    ];
                    dispatch(updateMapZoom(12));
                    dispatch(updateMapCenter(centroidCoordinates));
                    changeMapZoom(12);
                    changeMapCenter(centroidCoordinates);

                    let styleIrisLayer = {
                        color: "#364F6B",
                        weight: 4,
                        opacity: 0.6,
                    };
                    irisLayer = L.geoJSON(feature, {
                        style: styleIrisLayer,
                    });

                    let popupIrisLayer = `<div class="iris-popup-container">
                    <div class="iris-popup-line">
                      <span class="iris-popup-title">City : </span>
                      <span>${featureProperties.city}</span>
                    </div>
                    <div class="iris-popup-line">
                      <span class="iris-popup-title">Citycode : </span>
                      <span>${featureProperties.citycode}</span>
                    </div>
                    <div class="iris-popup-line">
                      <span class="iris-popup-title">Name : </span>
                      <span>${featureProperties.name}</span>
                    </div>
                    <div class="iris-popup-line">
                      <span class="iris-popup-title">Type : </span>
                      <span>${featureProperties.type}</span>
                    </div>
                    <div class="iris-popup-line">
                      <span class="iris-popup-title">Iris : </span>
                      <span>${featureProperties.iris}</span>
                    </div>
                    </div>`;

                    irisLayer
                        .bindPopup(popupIrisLayer, { offset: [100, 80] })
                        .on("mouseover", function (e) {
                            this.openPopup();
                        })
                        .on("mouseout", function (e) {
                            this.closePopup();
                        });
                    irisLayer.addTo(mapRef.current);

                    // Don't use the geocoder attributes stored in the session storage
                    // Use those recovered from the api
                    if (useGeocoderAttributesSessionStorage === false) {
                        addGeocoderMarkerLayer(
                            geocoderCoordinates,
                            geocoderLabel
                        );
                    } else {
                        let sessionStorageGeocoderAddress = sessionStorage.getItem("geocoderAddress");
                        let sessionStorageGeocoderLongitude = sessionStorage.getItem("geocoderLongitude");
                        let sessionStorageGeocoderLatitude = sessionStorage.getItem("geocoderLatitude");
                        if (sessionStorageGeocoderAddress !== "") {
                            addGeocoderMarkerLayer(
                                [
                                    sessionStorageGeocoderLongitude,
                                    sessionStorageGeocoderLatitude
                                ],
                                sessionStorageGeocoderAddress
                            );
                        }

                    }

                    // Update of the current iris code -> update the width of the map and the dashboard
                    dispatch(
                        updateIrisCodes({
                            currentCode: featureProperties.iris,
                            currentCompleteCode:
                                featureProperties.complete_code,
                        })
                    );
                }
            });
    };

    return (
        <div className="map-container">
            <div id="map" ref={mapRef} height={"100%"}></div>
        </div>
    );
}

export default withResizeDetector(MapWrapper);
