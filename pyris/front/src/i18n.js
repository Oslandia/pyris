import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";

const { api_url, url_suffix, defaultLanguage } = require("./settings");

i18n
    // detect user language
    // learn more: https://github.com/i18next/i18next-browser-languageDetector
    .use(LanguageDetector)
    // pass the i18n instance to react-i18next.
    .use(initReactI18next)
    // init i18next
    // for all options read: https://www.i18next.com/overview/configuration-options
    .init({
        debug: true,
        fallbackLng: defaultLanguage,
        interpolation: {
            escapeValue: false,
        },
        resources: {
            fr: {
                translation: {
                    description: {
                        languageFrench: "Français",
                        languageEnglish: "Anglais",
                        tabHome: "Accueil",
                        tabDashboard: "Tableau de bord",
                        urlTabHome: "accueil",
                        urlTabDashboard: "tableau-de-bord",
                        homeTitleMain: "Pyris: Insee IRIS Geolocalizer",
                        homeTitleDescription: "Description",
                        homeTextDescription: `<p className="sub-title">
                        Trouver un code IRIS à partir d'une adresse française :
                    </p>
                    <p className="paragraph">
                        <a
                            href="http://www.insee.fr/fr/methodes/default.asp?page=definitions/iris.htm"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            IRIS
                        </a>
                        est un code relatif à un district fourni par
                        <a
                            href="https://fr.wikipedia.org/wiki/Institut_national_de_la_statistique_et_des_%C3%A9tudes_%C3%A9conomiques"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            l'Insee,
                        </a>
                        Institut National de la Statistique et des Etudes Economiques.
                        Ainsi, il est possible d'avoir des indicateurs statistiques pour plus de 50 000 IRIS (resp. districts).
                        Les coordonnées longitude/latitude sont récupérées grâce à la base de données
                        <a
                            href="https://adresse.data.gouv.fr/"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                          Base Adresse Nationale Française
                        </a>
                        et son excellente
                        <a
                            href="https://adresse.data.gouv.fr/api/"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            API.
                        </a>
                    </p>`,
                        homeTitleInteractiveDashboard:
                            "Tableau de bord interactif",
                        homeTextInteractiveDashboard: `<p className="paragraph">
                        Rechercher un IRIS à partir d'une adresse et le visualiser sur une
                        <a
                            href=${url_suffix}/fr/tableau-de-bord
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            carte interactive !!
                        </a>
                    </p>
                    <p className="paragraph">
                    Pour chaque IRIS, vous pouvez trouver des statistiques sur la population,
                    l'emploi et le logement sous la forme de
                    <a>graphiques</a>. Ces données proviennent de l'Insee.
                    </p>
                    <p className="paragraph">
                        Voici un exemple pour le
                        <a href=${url_suffix}/fr/tableau-de-bord/751010301>
                        premier arrondissement de Paris "Palais Royal".
                        </a>
                    </p>`,
                        homeTitleData: "Données",
                        homeTextData: `<p className="paragraph">
                        Les données brutes sont disponibles sur les sites de
                        <a
                            href="https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#contoursiris"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            l'IGN
                        </a>
                        et de
                        <a
                            href="https://www.insee.fr/fr/statistiques"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            l'Insee.
                        </a>
                    </p>`,
                        homeTitleCode: "Code",
                        homeTextCode: `                    <p className="paragraph">
                        Vous pouvez trouver le code source de ce projet sur la                         <a
                        href="https://gitlab.com/Oslandia/pyris"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                    page Gitlab de pyris.
                    </a>
                    Les principales technologies utilisées sont Python, Flask, PostgreSQL et React.
                    </p>`,
                        homeTitleAccess: "Accès",
                        homeTextAccess: `<p className="sub-title">Exemples :</p>
                        <ul>
                            <li className="paragraph">
                                Obtenir des informations à partir d'un code IRIS :
                                <a
                                    href="${api_url}/iris/0508?"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                ${api_url}/iris/0508?
                                </a>
                            </li>
                            <li className="paragraph">
                                Récupérer le code IRIS d'une adresse:
                                <a
                                    href="${api_url}/api/search/?q=place de la bourse Bordeaux"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                ${api_url}/search/?q=place de la
                                    bourse Bordeaux
                                </a>
                            </li>
                        </ul>
                        <p className="paragraph">
                             Vous pouvez également lire et essayer
                            <a
                                href="http://127.0.0.1:5555/doc/"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                l'API REST
                            </a>
                            générée par
                            <a
                                href="https://flask-restplus.readthedocs.org/en/stable/"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Flask-RESTPlus
                            </a>
                            et
                            <a
                                href="http://swagger.io/"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Swagger.
                            </a>
                        </p>

                        <p className="terms-of-use-container paragraph">

                            <b>Termes d'utilisation :</b> Il s'agit d'une instance de démonstration.
                            Veuillez ne pas l'utiliser en production.
                            Vous pouvez tester et prototyper, mais pour une instance de production,
                            veuillez installer votre propre instance ou nous contacter.
                        </p>`,
                        mapGeocoding: "Rechercher une adresse",
                        dashboardIris: "Iris",
                        dashboardPopulation: "Population",
                        dashboardHousing: "Logement",
                        dashboardEmployment: "Emploi",
                        chartIrisCode: "Code :",
                        chartIrisName: "Nom :",
                        chartIrisCity: "Commune :",
                        chartIrisCitycode: "Code commune :",
                        chartIrisType: "Type :",
                        chartPopulationTitle: "Population",
                        chartPopulationLabelFemale: "Femme",
                        chartPopulationLabelMale: "Homme",
                        chartPopulationAgeTitle: "Population par tranche d'âge",
                        chartPopulationAgeSexTitle:
                            "Population par tranche d'âge et par sexe",
                        chartHousingTypeTitle: "Type",
                        chartHousingTypeLabelTotal: "Total",
                        chartHousingTypeLabelAppartment: "Appartement",
                        chartHousingTypeLabelHouse: "Maison",
                        chartHousingTypeLabelMain: "Principal",
                        chartHousingTypeLabelSecond: "Secondaire",
                        chartHousingTypeLabelUnoccupied: "Inoccupé",
                        chartHousingYearTitle: "Année",
                        chartHousingTypeLabel1Room: "1 pièce",
                        chartHousingTypeLabel2Room: "2 pièces",
                        chartHousingTypeLabel3Room: "3 pièces",
                        chartHousingTypeLabel4Room: "4 pièces",
                        chartHousingRoomTitle: "Pièce",
                        chartHousingAreaTitle: "Surface",
                        dashboardEmploymentPopulation1564: "Population 15-64 :",
                        dashboardEmploymentActive: "Actifs :",
                        dashboardEmploymentUnemployed: "Sans emploi :",
                        dashboardEmploymentUnemploymentRate:
                            "Taux de chômage :",
                        chartEmploymentTitle: "Emploi",
                        chartEmploymentLabelActive: "Actifs",
                        chartEmploymentLabelInactive: "Inactifs",
                        chartEmploymentLabelUnemployed: "Sans emploi",
                        chartEmploymentLabelStudent: "Etudiants",
                        chartEmploymentLabelRetired: "Retraités",
                        chartEmploymentSexTitle: "Emploi par sexe",
                        chartEmploymentSexLabelFemale: "Femme",
                        chartEmploymentSexLabelMale: "Homme",
                        chartEmploymentAgeTitle: "Emploi par âge",
                        chartEmploymentAgeLabelActive: "Actifs",
                        chartEmploymentAgeLabelUnemployed: "Sans emplois",
                    },
                },
            },
            en: {
                translation: {
                    description: {
                        languageFrench: "French",
                        languageEnglish: "English",
                        tabHome: "Home",
                        tabDashboard: "Dashboard",
                        urlTabHome: "accueil",
                        urlTabDashboard: "tableau-de-bord",
                        homeTitleMain: "Pyris: Insee IRIS Geolocalizer",
                        homeTitleDescription: "Description",
                        homeTextDescription: `<p className="sub-title">
                        Find an IRIS code from a French address:
                    </p>
                    <p className="paragraph">
                        <a
                            href="http://www.insee.fr/fr/methodes/default.asp?page=definitions/iris.htm"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            IRIS
                        </a>
                        is a code related to a district provided by
                        <a
                            href="https://fr.wikipedia.org/wiki/Institut_national_de_la_statistique_et_des_%C3%A9tudes_%C3%A9conomiques"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Insee
                        </a>
                        , the French National Institute of Statistics and
                        Economic Studies. Thus, it's possible to have some
                        statistical indicators for more than 50,000 different
                        IRIS (resp. districts). The longitude/latitude
                        coordinates are retrieved thanks to the
                        <a
                            href="https://adresse.data.gouv.fr/"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            National Address French Base
                        </a>
                        and its great
                        <a
                            href="https://adresse.data.gouv.fr/api/"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            API
                        </a>
                        .
                    </p>`,
                        homeTitleInteractiveDashboard: "Interactive Dashboard",
                        homeTextInteractiveDashboard: `<p className="paragraph">
                        Search an IRIS from an address and visualize it on an
                        <a
                            href=${url_suffix}/en/tableau-de-bord
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            interactive map!!
                        </a>
                    </p>
                    <p className="paragraph">
                        For each IRIS, you can find some population, employment
                        and housing stats represented in the form of
                        <a>charts</a>. There data came from Insee.
                    </p>
                    <p className="paragraph">
                        Here an example in the
                        <a href=${url_suffix}/en/tableau-de-bord/751010301>
                            first Paris district "Palais Royal"
                        </a>
                        .
                    </p>`,
                        homeTitleData: "Data",
                        homeTextData: `<p className="paragraph">
                        Find the raw data at the
                        <a
                            href="https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#contoursiris"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            IGN website
                        </a>
                        and the
                        <a
                            href="https://www.insee.fr/fr/statistiques"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Insee
                        </a>
                        website.
                    </p>`,
                        homeTitleCode: "Code",
                        homeTextCode: `                    <p className="paragraph">
                        You can find the source code of this project on the
                        <a
                            href="https://gitlab.com/Oslandia/pyris"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            pyris Gitlab page
                        </a>
                        . It's powered by Python, Flask, PostgreSQL and React.
                    </p>`,
                        homeTitleAccess: "Access",
                        homeTextAccess: `                    <p className="sub-title">Examples:</p>
                        <ul>
                            <li className="paragraph">
                                Get some information from an IRIS code:
                                <a
                                    href="http://data.oslandia.io/pyris/api/iris/0508?"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    http://data.oslandia.io/pyris/api/iris/0508?
                                </a>
                            </li>
                            <li className="paragraph">
                                Retrieve the IRIS code from an address:
                                <a
                                    href="http://data.oslandia.io/pyris/api/search/?q=place de la bourse Bordeaux"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    http://data.oslandia.io/pyris/api/search/?q=place de la
                                    bourse Bordeaux
                                </a>
                            </li>
                        </ul>
                        <p className="paragraph">
                            You also can read and try
                            <a
                                href="http://127.0.0.1:5555/doc/"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                the REST API
                            </a>
                            generated by
                            <a
                                href="https://flask-restplus.readthedocs.org/en/stable/"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Flask-RESTPlus
                            </a>
                            and
                            <a
                                href="http://swagger.io/"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Swagger
                            </a>
                            .
                        </p>

                        <p className="terms-of-use-container paragraph">

                            <b>Terms of Use:</b> This is a demo instance, please
                            don't use in production. You can test and prototype, but
                            for a production instance please install your own or
                            contact us.
                        </p>`,
                        mapGeocoding: "Search for an address",
                        dashboardIris: "Iris",
                        dashboardPopulation: "Population",
                        dashboardHousing: "Housing",
                        dashboardEmployment: "Employment",
                        chartIrisCode: "Code:",
                        chartIrisName: "Name:",
                        chartIrisCity: "City:",
                        chartIrisCitycode: "Citycode:",
                        chartIrisType: "Type:",
                        chartPopulationTitle: "Population",
                        chartPopulationLabelFemale: "Female",
                        chartPopulationLabelMale: "Male",
                        chartPopulationAgeTitle: "Population by age",
                        chartPopulationAgeSexTitle: "Population by age and sex",
                        chartHousingTypeTitle: "Type",
                        chartHousingTypeLabelTotal: "Total",
                        chartHousingTypeLabelAppartment: "Appartment",
                        chartHousingTypeLabelHouse: "House",
                        chartHousingTypeLabelMain: "Main",
                        chartHousingTypeLabelSecond: "Second",
                        chartHousingTypeLabelUnoccupied: "Unoccupied",
                        chartHousingYearTitle: "Year",
                        chartHousingTypeLabel1Room: "1 room",
                        chartHousingTypeLabel2Room: "2 room",
                        chartHousingTypeLabel3Room: "3 room",
                        chartHousingTypeLabel4Room: "4 room",
                        chartHousingRoomTitle: "Room",
                        chartHousingAreaTitle: "Area",
                        dashboardEmploymentPopulation1564: "Population 15-64:",
                        dashboardEmploymentActive: "Active:",
                        dashboardEmploymentUnemployed: "Unemployed:",
                        dashboardEmploymentUnemploymentRate:
                            "Unemployment rate:",
                        chartEmploymentTitle: "Employment",
                        chartEmploymentLabelActive: "Active",
                        chartEmploymentLabelInactive: "Inactive",
                        chartEmploymentLabelUnemployed: "Unemployed",
                        chartEmploymentLabelStudent: "Student",
                        chartEmploymentLabelRetired: "Retired",
                        chartEmploymentSexTitle: "Employment by sex",
                        chartEmploymentSexLabelFemale: "Female",
                        chartEmploymentSexLabelMale: "Male",
                        chartEmploymentAgeTitle: "Employment by age",
                        chartEmploymentAgeLabelActive: "Active",
                        chartEmploymentAgeLabelUnemployed: "Unemployed",
                    },
                },
            },
        },
    });

export default i18n;
