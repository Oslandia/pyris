const api_url = process.env.REACT_APP_API_HOST;
const url_suffix = process.env.REACT_APP_URL_SUFFIX;
const defaultLanguage = "fr";
const availableLanguages = ["fr", "en"];

const config = {
    map: {
        center: [47.5, 2],
        zoom: 6,
    },
    colors: {
        backgroundColor: [
            "rgba(54, 79, 107, 0.7)",
            "rgba(63, 193, 201, 0.7)",
            "rgba(252, 81, 133, 0.7)",
            "rgba(255, 222, 125, 0.7)",
            "rgba(15, 76, 117, 0.7)",
            "rgba(187, 225, 250, 0.7)",
            "rgba(50, 130, 184, 0.7)",
            "rgba(248, 243, 212, 1)",
        ],
        borderColor: [
            "rgb(54, 79, 107)",
            "rgb(63, 193, 201)",
            "rgb(252, 81, 133)",
            "rgb(255, 222, 125)",
            "rgb(15, 76, 117)",
            "rgb(187, 225, 250)",
            "rgb(50, 130, 184)",
            "rgb(248, 243, 212)",
        ],
    },
};

module.exports = {
    api_url,
    url_suffix,
    config,
    availableLanguages,
    defaultLanguage,
};
