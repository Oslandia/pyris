import { configureStore } from "@reduxjs/toolkit";
import Map from "./stores/Map";
import Iris from "./stores/Iris";
import Charts from "./stores/Charts";
import Dashboards from "./stores/Dashboards";
import KeyFigures from "./stores/KeyFigures";

export default configureStore({
  reducer: {
    map: Map,
    iris: Iris,
    charts: Charts,
    dashboards: Dashboards,
    keyfigures: KeyFigures,
  },
});
