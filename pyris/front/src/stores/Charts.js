import { createSlice } from "@reduxjs/toolkit";
import i18n from "../i18n";

const { api_url, config } = require("../settings");
const configColors = config.colors;
const configColorsBackgroundColor = configColors.backgroundColor;
const configColorsBorderColor = configColors.borderColor;

export const charts = createSlice({
    name: "charts",
    initialState: {
        Population: {
            charts: {
                population: {
                    container: {
                        height: "21",
                        width: "14",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t("description.chartPopulationTitle"),
                    titleDisplay: "true",
                    legendDisplay: "false",
                    maintainAspectRatio: false,
                    labelsChart: [
                        i18n.t("description.chartPopulationLabelFemale"),
                        i18n.t("description.chartPopulationLabelMale"),
                    ],
                    datasets: [
                        {
                            label: "",
                            data: [],
                            url: api_url + "/insee/population/",
                            urlParams: "",
                            backgroundColor: configColorsBackgroundColor[3],
                            borderColor: configColorsBorderColor[3],
                            borderWidth: 1,
                        },
                    ],
                },
                population_age: {
                    container: {
                        height: "21",
                        width: "26",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t("description.chartPopulationAgeTitle"),
                    titleDisplay: "true",
                    legendDisplay: "false",
                    maintainAspectRatio: false,
                    labelsChart: [
                        "0-14",
                        "15-29",
                        "30-44",
                        "45-59",
                        "60-75",
                        ">75",
                    ],
                    datasets: [
                        {
                            label: "",
                            data: [],
                            url: api_url + "/insee/population/distribution/",
                            urlParams: "?by=age",
                            backgroundColor: configColorsBackgroundColor[1],
                            borderColor: configColorsBorderColor[1],
                            borderWidth: 1,
                        },
                    ],
                },
                population_age_sex: {
                    container: {
                        height: "27",
                        width: "42",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t(
                        "description.chartPopulationAgeSexTitle"
                    ),
                    titleDisplay: "true",
                    legendDisplay: "true",
                    maintainAspectRatio: false,
                    labelsChart: [
                        "0-14",
                        "15-29",
                        "30-44",
                        "45-59",
                        "60-75",
                        ">75",
                    ],
                    datasets: [
                        // Female
                        {
                            label: i18n.t(
                                "description.chartPopulationLabelFemale"
                            ),
                            data: [],
                            url: api_url + "/insee/population/distribution/",
                            urlParams: "?by=sex",
                            backgroundColor: configColorsBackgroundColor[0],
                            borderColor: configColorsBorderColor[0],
                            borderWidth: 1,
                        },
                        // Male
                        {
                            label: i18n.t(
                                "description.chartPopulationLabelMale"
                            ),
                            data: [],
                            url: api_url + "/insee/population/distribution/",
                            urlParams: "?by=sex",
                            backgroundColor: configColorsBackgroundColor[1],
                            borderColor: configColorsBorderColor[1],
                            borderWidth: 1,
                        },
                    ],
                },
            },
        },
        Housing: {
            charts: {
                type: {
                    container: {
                        height: "25",
                        width: "16",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t("description.chartHousingTypeTitle"),
                    titleDisplay: "true",
                    legendDisplay: "false",
                    maintainAspectRatio: false,
                    labelsChart: [
                        i18n.t("description.chartHousingTypeLabelTotal"),
                        i18n.t("description.chartHousingTypeLabelAppartment"),
                        i18n.t("description.chartHousingTypeLabelHouse"),
                        i18n.t("description.chartHousingTypeLabelMain"),
                        i18n.t("description.chartHousingTypeLabelSecond"),
                        i18n.t("description.chartHousingTypeLabelUnoccupied"),
                    ],
                    datasets: [
                        {
                            label: "",
                            data: [],
                            url: api_url + "/insee/logement/",
                            urlParams: "",
                            backgroundColor: [
                                configColorsBackgroundColor[0],
                                configColorsBackgroundColor[1],
                                configColorsBackgroundColor[1],
                                configColorsBackgroundColor[3],
                                configColorsBackgroundColor[3],
                                configColorsBackgroundColor[3],
                            ],
                            borderColor: [
                                configColorsBorderColor[0],
                                configColorsBorderColor[1],
                                configColorsBorderColor[1],
                                configColorsBorderColor[3],
                                configColorsBorderColor[3],
                                configColorsBorderColor[3],
                            ],
                            borderWidth: 1,
                        },
                    ],
                },
                year: {
                    container: {
                        height: "25",
                        width: "16",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t("description.chartHousingYearTitle"),
                    titleDisplay: "true",
                    legendDisplay: "false",
                    maintainAspectRatio: false,
                    labelsChart: [
                        "< 1919",
                        "1920-1945",
                        "1946-1970",
                        "1971-1990",
                        "1991-2005",
                        "2006-2011",
                    ],
                    datasets: [
                        {
                            label: "",
                            data: [],
                            url: api_url + "/insee/logement/distribution/",
                            urlParams: "?by=year",
                            backgroundColor: configColorsBackgroundColor[1],
                            borderColor: configColorsBorderColor[1],
                            borderWidth: 1,
                        },
                    ],
                },
                room: {
                    container: {
                        height: "25",
                        width: "16",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t("description.chartHousingRoomTitle"),
                    titleDisplay: "true",
                    legendDisplay: "false",
                    maintainAspectRatio: false,
                    labelsChart: [
                        i18n.t("description.chartHousingTypeLabel1Room"),
                        i18n.t("description.chartHousingTypeLabel2Room"),
                        i18n.t("description.chartHousingTypeLabel3Room"),
                        i18n.t("description.chartHousingTypeLabel4Room"),
                        ">5",
                    ],
                    datasets: [
                        {
                            label: "",
                            data: [],
                            url: api_url + "/insee/logement/distribution/",
                            urlParams: "?by=room",
                            backgroundColor: configColorsBackgroundColor[0],
                            borderColor: configColorsBorderColor[0],
                            borderWidth: 1,
                        },
                    ],
                },
                area: {
                    container: {
                        height: "25",
                        width: "17",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t("description.chartHousingAreaTitle"),
                    titleDisplay: "true",
                    legendDisplay: "false",
                    maintainAspectRatio: false,
                    labelsChart: [
                        "< 30m2",
                        "30-40m2",
                        "40-60m2",
                        "60-80m2",
                        "80-100m2",
                        "100-120m2",
                        "> 120m2",
                    ],
                    datasets: [
                        {
                            label: "",
                            data: [],
                            url: api_url + "/insee/logement/distribution/",
                            urlParams: "?by=area",
                            backgroundColor: configColorsBackgroundColor[3],
                            borderColor: configColorsBorderColor[3],
                            borderWidth: 1,
                        },
                    ],
                },
            },
        },
        Employment: {
            charts: {
                employment: {
                    container: {
                        height: "22",
                        width: "20.5",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t("description.chartEmploymentTitle"),
                    titleDisplay: "true",
                    legendDisplay: "false",
                    maintainAspectRatio: false,
                    labelsChart: [
                        i18n.t("description.chartEmploymentLabelActive"),
                        i18n.t("description.chartEmploymentLabelInactive"),
                        i18n.t("description.chartEmploymentLabelUnemployed"),
                        i18n.t("description.chartEmploymentLabelStudent"),
                        i18n.t("description.chartEmploymentLabelRetired"),
                    ],
                    datasets: [
                        {
                            label: "",
                            data: [],
                            url: api_url + "/insee/activite/",
                            urlParams: "",
                            backgroundColor: configColorsBackgroundColor[0],
                            borderColor: configColorsBorderColor[0],
                            borderWidth: 1,
                        },
                    ],
                },
                employment_sex: {
                    container: {
                        height: "26",
                        width: "20.5",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t("description.chartEmploymentSexTitle"),
                    titleDisplay: "true",
                    legendDisplay: "true",
                    maintainAspectRatio: false,
                    labelsChart: [
                        i18n.t("description.chartEmploymentLabelActive"),
                        i18n.t("description.chartEmploymentLabelInactive"),
                        i18n.t("description.chartEmploymentLabelUnemployed"),
                        i18n.t("description.chartEmploymentLabelStudent"),
                        i18n.t("description.chartEmploymentLabelRetired"),
                    ],
                    datasets: [
                        // Female
                        {
                            label: i18n.t("description.chartEmploymentSexLabelFemale"),
                            data: [],
                            url: api_url + "/insee/activite/distribution/",
                            urlParams: "?by=sex",
                            backgroundColor: configColorsBackgroundColor[3],
                            borderColor: configColorsBorderColor[3],
                            borderWidth: 1,
                        },
                        // Male
                        {
                            label: i18n.t("description.chartEmploymentSexLabelMale"),
                            data: [],
                            url: api_url + "/insee/activite/distribution/",
                            urlParams: "?by=sex",
                            backgroundColor: configColorsBackgroundColor[1],
                            borderColor: configColorsBorderColor[1],
                            borderWidth: 1,
                        },
                    ],
                },
                employment_age: {
                    container: {
                        height: "28",
                        width: "20.5",
                    },
                    typeChart: "bar",
                    titleChart: i18n.t("description.chartEmploymentAgeTitle"),
                    titleDisplay: "true",
                    legendDisplay: "true",
                    maintainAspectRatio: false,
                    labelsChart: ["15-24", "25-54", "55-64"],
                    datasets: [
                        // Active
                        {
                            label: i18n.t("description.chartEmploymentAgeLabelActive"),
                            data: [],
                            url: api_url + "/insee/activite/distribution/",
                            urlParams: "?by=age",
                            backgroundColor: configColorsBackgroundColor[0],
                            borderColor: configColorsBorderColor[0],
                            borderWidth: 1,
                        },
                        // Unemployed
                        {
                            label: i18n.t("description.chartEmploymentAgeLabelUnemployed"),
                            data: [],
                            url: api_url + "/insee/activite/distribution/",
                            urlParams: "?by=age",
                            backgroundColor: configColorsBackgroundColor[1],
                            borderColor: configColorsBorderColor[1],
                            borderWidth: 1,
                        },
                    ],
                },
            },
        },
    },
    reducers: {
        updateChartData: (state, props) => {
            const payload = props.payload;
            state[payload.section]["charts"][payload.chart]["datasets"][
                payload.index
            ]["data"] = payload.data;
        },
    },
});

export const { updateChartData } = charts.actions;

export default charts.reducer;
