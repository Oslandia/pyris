import { createSlice } from "@reduxjs/toolkit";

export const dashboards = createSlice({
    name: "dashboards",
    initialState: {
        Iris: {
            containers: {
                one: { height: "17.5", width: "26", overflow: "auto" },
            },
        },
        Population: {
            containers: {
                one: { height: "20", width: "42" },
                two: { height: "30", width: "42" },
            },
        },
        Housing: {
            containers: {
                one: { height: "25", width: "72" },
            },
        },
        Employment: {
            containers: {
                one: { height: "85", width: "20.5" },
            },
        },
    },
    reducers: {},
});

export default dashboards.reducer;
