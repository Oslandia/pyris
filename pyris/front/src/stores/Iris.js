import { createSlice } from "@reduxjs/toolkit";

export const iris = createSlice({
  name: "iris",
  initialState: {
    currentCode: "",
    currentCompleteCode: "",
    // currentCompleteCode: "870850901", // for test
  },
  reducers: {
    updateIrisCodes: (state, props) => {
        const payload = props.payload;
        state.currentCode = payload.currentCode;
        state.currentCompleteCode = payload.currentCompleteCode;
    }
  },
});

export const { updateIrisCodes } = iris.actions;

export default iris.reducer;
