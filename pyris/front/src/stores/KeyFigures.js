import { createSlice } from "@reduxjs/toolkit";

export const keyFigures = createSlice({
    name: "keyFigures",
    initialState: {
        Iris: {
            keyFigures: {
                one: { height: "" },
            },
        },
        Population: {
            keyFigures: {
                one: { height: "" },
            },
        },
        Housing: {
            keyFigures: {
                one: { height: "" },
            },
        },
        Employment: {
            keyFigures: {
                one: { height: "15", overflow: "auto" },
            },
        },
    },
    reducers: {},
});

export default keyFigures.reducer;
