import setuptools

from pyris import __version__

with open("README.rst") as fobj:
    LONG_DESCRIPTION = fobj.read()

INSTALL_REQUIRES = [
    "Flask>=3,<4",
    "flask-restx>=1,<2",
    "Flask-Cors>=3,<4",
    "Werkzeug>=3,<4",
    "psycopg>=3,<4",
    "pyaml",
    "slumber",
    "gunicorn==19.9",
]

setuptools.setup(
    name="pyris",
    version=__version__,
    license="BSD",
    url="https://gitlab.com/Oslandia/pyris",
    packages=setuptools.find_packages(exclude=["scripts-data", "app.yml"]),
    include_package_data=True,
    install_requires=INSTALL_REQUIRES,
    # pip install -e .[dev]
    extras_require={"dev": ["pytest", "pytest-sugar", "ipython", "ipdb"]},
    author="Damien Garaud",
    author_email="damien.garaud@gmail.com",
    description="INSEE/IRIS geolocalization",
    long_description=LONG_DESCRIPTION,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
)
