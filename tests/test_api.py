import pytest

from pyris.api import create_app

app = create_app()


@pytest.fixture
def client():
    yield app.test_client()


def test_app_index(client):
    resp = client.get("/api/")
    assert resp.status_code == 200


def test_app_swagger_json(client):
    resp = client.get("/api/swagger.json")
    assert resp.status_code == 200


def test_api_iris_from_city(client):
    lyon_premier_arr_code = "69381"
    resp = client.get("/api/city/code/" + lyon_premier_arr_code)
    assert resp.status_code == 200
    assert len(resp.json) > 5


def test_api_iris(client):
    code_iris = "693810101"
    resp = client.get("/api/compiris/" + code_iris)
    assert resp.status_code == 200
    data = resp.json
    assert data["type"] == "H"
    assert data["citycode"] == "69381"
    resp = client.get("/api/iris/" + "0101")
    assert resp.status_code == 200
    data = resp.json
    assert len(data) == 10


def test_api_coords(client):
    lon, lat = 4.836098, 45.765926
    resp = client.get("/api/coords", query_string={"lat": lat, "lon": lon})
    assert resp.status_code == 200
    data = resp.json
    assert data["complete_code"] == "693810101"


def test_api_address_search(client):
    """note: this route depends on a remote service dedicated to address searching"""
    query = "11 rue république Lyon"
    resp = client.get("/api/search/", query_string={"q": query})
    assert resp.status_code == 200
    data = resp.json
    assert data["complete_code"] == "693810101"


def test_api_special_character(client):
    query = "[11 rue de la république Lyon"
    resp = client.get("/api/search/", query_string={"q": query})
    assert resp.status_code == 404
    data = resp.json
    assert data["message"].startswith("No address found matching that query")


def test_api_address_search_with_empty_results(client):
    # e.g. with DOM-TOM
    resp = client.get(
        "/api/search/", query_string={"q": "rue Kassim", "citycode": "97610"}
    )  # Mayotte
    assert resp.status_code == 404
    data = resp.json
    assert data == {
        "message": "IRIS not found from coordinates (45.207327, -12.736056)"
    }


def test_api_city_search(client):
    resp = client.get(
        "/api/city/search/", query_string={"q": "bordeaux", "citycode": "33063"}
    )
    assert resp.status_code == 200
    data = resp.json
    assert data["city_name"].lower() == "bordeaux"
    assert len(data["iris_list"]) > 33


def test_api_city_search_with_empty_results(client):
    # e.g. with DOM-TOM
    resp = client.get(
        "/api/city/search/", query_string={"q": "koungou", "citycode": "97610"}
    )  # Mayotte
    assert resp.status_code == 404
    data = resp.json
    assert data == {
        "message": "IRIS not found from coordinates (-12.737452, 45.196571)"
    }


def test_api_insee_population(client):
    code_iris = "693810101"
    resp = client.get("/api/insee/population/" + code_iris)
    assert resp.status_code == 200
    data = resp.json
    assert data["iris"] == code_iris
    assert data["citycode"] == "69381"
    assert "population" in data


def test_api_insee_population_distribution(client):
    code_iris = "693810101"
    # by age
    resp = client.get(
        "/api/insee/population/distribution/" + code_iris, query_string={"by": "age"}
    )
    assert resp.status_code == 200
    data = resp.json
    assert data["iris"] == code_iris
    assert data["citycode"] == "69381"
    assert "population_15_29" in data["data"]

    # by sex
    resp = client.get(
        "/api/insee/population/distribution/" + code_iris, query_string={"by": "sex"}
    )
    assert resp.status_code == 200
    data = resp.json
    assert data["iris"] == code_iris
    assert data["citycode"] == "69381"
    assert "female_15_29" in data["data"]


def test_api_insee_logement(client):
    code_iris = "693810101"
    resp = client.get("/api/insee/logement/" + code_iris)
    assert resp.status_code == 200
    data = resp.json
    assert data["iris"] == code_iris
    assert data["citycode"] == "69381"
    assert "logement" in data


def test_api_insee_logement_distribution(client):
    code_iris = "693810101"

    # by room
    resp = client.get(
        "/api/insee/logement/distribution/" + code_iris, query_string={"by": "room"}
    )
    assert resp.status_code == 200
    data = resp.json
    assert data["iris"] == code_iris
    assert data["citycode"] == "69381"
    assert "main_residence_2_room" in data["data"]


def test_api_insee_activite(client):
    code_iris = "693810101"
    resp = client.get("/api/insee/activite/" + code_iris)
    assert resp.status_code == 200
    data = resp.json
    assert data["iris"] == code_iris
    assert data["citycode"] == "69381"
    assert "actif" in data
    assert "chomeur" in data


# FIXME: pytest extract:
# psycopg2.errors.UndefinedColumn: ERREUR:  la colonne « p_hchom1564 » n'existe pas
# E               LINE 8:   ,p_hchom1564 as homme_chomage
# E                          ^
# E               HINT:  Peut-être que vous souhaitiez référencer la colonne « activite.p_chom1564 ».
# Seems related to https://gitlab.com/Oslandia/pyris/-/issues/50
#
# def test_api_insee_activite_distribution(client):
#     code_iris = "693810101"
#     # by age
#     resp = client.get(
#         "/api/insee/activite/distribution/" + code_iris, query_string={"by": "age"}
#     )
#     assert resp.status_code == 200
#     data = resp.json
#     assert data["iris"] == code_iris
#     assert data["citycode"] == "69381"
#     assert "actif_25_54" in data["data"]

#     # by sex
#     resp = client.get(
#         "/api/insee/activite/distribution/" + code_iris, query_string={"by": "sex"}
#     )
#     assert resp.status_code == 200
#     data = resp.json
#     assert data["iris"] == code_iris
#     assert data["citycode"] == "69381"
#     assert "femme_actif" in data["data"]
